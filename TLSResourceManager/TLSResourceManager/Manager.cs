﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TLSResourceManager
{
    public class Manager
    {

        private const string configFile = @"config.cfg";

        private string[] iOSResourceFile;
        private string[] androidDir;
        private Dictionary<string, string> configuration;
        private Dictionary<string, string> androidResourceXmlFile;
        private List<XElement> androidResources;
        private List<string> androidMissingResources;
        private List<string> iOSRepeatedResourceEntries;

        public bool Run()
        {
            bool result = false;
            configuration = new Dictionary<string, string>();
            LoadConfigurations();
            if (!configuration.Any())
            {
                Console.WriteLine("Error, configuration file not found!\nPlease check configuration file: " + configFile);
                return false;
            }

            PrepareOutputLists();
            ReadiOSResourceFile();
            ReadAndroidResourceFile(configuration["androidResourceFileDir"] + "\\" + configuration["androidResourceFileName"]);
            if (iOSResourceFile == null || androidResources == null)
            {
                Console.WriteLine("Error, no files to compare were found");
                return false;
            }

            PrepareKeyValueDict();
            result = CompareResources();
            CreateAndroidDefaultResourceFile();

            if (result)
                Console.WriteLine("Please check files in: " + configuration["targetDir"]);

            Console.WriteLine("\n\nOperation done!");
            Console.ReadLine();

            return result;
        }

        private void LoadConfigurations()
        {
            List<string> tempList = FileManager.ReadFile(configFile).ToList();
            if (!tempList.Any())
                return;
            foreach (string item in tempList)
            {
                string key = item.Substring(0, (item.IndexOf("=") - 1)).Trim();
                string value = item.Substring((item.IndexOf("=") + 1), (item.Length - (item.IndexOf("=") + 1))).Trim();
                configuration.Add(key, value);
            }
        }

        private bool CompareResources()
        {
            foreach (KeyValuePair<string, string> item in androidResourceXmlFile)
                if (!androidResources.Any(x => x.Attribute("name").Value.Equals(item.Key, StringComparison.InvariantCultureIgnoreCase)))
                {
                    string textLine = item.Key + " = " + item.Value;
                    androidMissingResources.Add(textLine);
                    androidResources.Add(new XElement("string", new XAttribute("name", item.Key), item.Value));
                }

            if (androidMissingResources.Count > 1)
                WriteResults();
            if (iOSRepeatedResourceEntries.Count > 1)
                WriteiOSDuplicates();
            return true;
        }

        private void PrepareOutputLists()
        {
            androidResourceXmlFile = new Dictionary<string, string>();
            androidMissingResources = new List<string>();
            iOSRepeatedResourceEntries = new List<string>();
            androidMissingResources.Add("Missing resources in Android listed below:\n\n");
            iOSRepeatedResourceEntries.Add("Detected repeated entries in iOS resources files listed below:\n\n");
        }

        private void PrepareKeyValueDict()
        {
            string lastKey = string.Empty;
            for (int i = 0; i < iOSResourceFile.Count(); i++)
            {
                if (string.IsNullOrEmpty(iOSResourceFile[i]) || iOSResourceFile[i].StartsWith(@"//") || iOSResourceFile[i].StartsWith("<!--"))
                    continue;

                int initialNumber = 0;
                int index = iOSResourceFile[i].IndexOf("=");
                string tempKey = string.Empty;
                string tempValue = string.Empty;
                if (index > 0)
                {
                    // KEY
                    tempKey = iOSResourceFile[i].Substring(0, index);
                    tempKey = tempKey.Replace("\"", string.Empty).Trim().Replace(' ', '_').Replace('.', '_')
                            .Replace(",", "_002c").Replace("&", "and").Replace("/", "_002f").Replace('-', '_')
                            .Replace("?", "_003f").Replace("'", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty)
                            .Replace("★", "_2605");

                    if (Int32.TryParse(tempKey[0].ToString(), out initialNumber))
                        tempKey = "_" + tempKey;

                    if (tempKey.EndsWith("!"))
                        tempKey = tempKey.Substring(0, (tempKey.Count() - 1)) + "_0021";
                    tempKey = tempKey.Trim();
                    if (string.IsNullOrWhiteSpace(tempKey))
                        continue;
                    lastKey = tempKey;

                    // VALUE - Diferent Languages
                    // NOT USED AT THE MOMENT
                    //tempValue = iOSResourceFile[i].Substring(index, (iOSResourceFile[i].Length - (index + 1)));
                    //tempValue = tempValue.Replace(";", string.Empty);
                    //tempValue = tempValue.Trim();


                    // VALUE - FOR DEFAULT ENGLISH
                    tempValue = iOSResourceFile[i].Substring(0, index).Replace("\"", string.Empty);
                    tempValue = tempValue.Trim();
                    if (androidResourceXmlFile.ContainsKey(tempKey))
                        iOSRepeatedResourceEntries.Add(iOSResourceFile[i]);
                    else
                        androidResourceXmlFile.Add(lastKey, tempValue);
                }
                else if (iOSResourceFile[i].EndsWith("\";") && !string.IsNullOrWhiteSpace(lastKey))
                {
                    // VALUE IN NEW LINE
                    tempValue = (iOSResourceFile[i]).Replace(";", string.Empty);
                    tempValue = tempValue.Trim();
                    androidResourceXmlFile[lastKey] = tempValue;
                }
            }
        }

        private void ReadiOSResourceFile()
        {
            iOSResourceFile = FileManager.ReadFile(configuration["iosResourceFileLocation"] + configuration["resourceFileName"]);
        }

        private void ReadAndroidResourceDir()
        {
            androidDir = FileManager.ScanDir(configuration["androidResourceFileDir"]);
        }

        private void ReadAndroidResourceFile(string location)
        {
            androidResources = FileManager.ReadXmlFile(location);
        }

        private void WriteResults()
        {
            FileManager.WriteToFile(configuration["targetDir"] + configuration["fileOperationResult"], androidMissingResources);
        }

        private void WriteiOSDuplicates()
        {
            FileManager.WriteToFile(configuration["targetDir"] + configuration["iOSDuplicatedResouceEntries"], iOSRepeatedResourceEntries);
        }

        private void CreateAndroidDefaultResourceFile()
        {
            StringBuilder xmlComment = new StringBuilder();
            xmlComment.Append("\n\tLoco xml export: Android string resources\n");
            xmlComment.Append("\tProject: tls\n");
            xmlComment.Append("\tRelease: Working copy\n");
            xmlComment.Append("\tLocale: en-EN, English\n");
            xmlComment.Append("\tExported by: heine mo\n");
            xmlComment.AppendFormat("\tExported at: {0}\n", DateTime.Now);
            XDocument doc =
                new XDocument(
                new XComment(xmlComment.ToString()),
                new XElement("resources",
                   androidResourceXmlFile.Select(x => new XElement("string", new XAttribute("name", x.Key), x.Value))
               )
           );
            doc.Save(configuration["targetDir"] + configuration["androidResourceFileName"]);

            //XDocument xDoc = new XDocument(androidResources);
            //xDoc.Element("resources").Add(androidResourceXmlFile.Select(x => new XElement("string", new XAttribute("name", x.Key), x.Value)));
        }
    }
}