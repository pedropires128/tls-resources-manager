﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TLSResourceManager
{
    public static class FileManager
    {
        public static string[] ReadFile(string location)
        {
            try
            {
                string[] result = File.ReadAllLines(location);
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't Read file from location: ", location);
                Console.WriteLine(ex);
            }
            return null;
        }

        public static List<XElement> ReadXmlFile(string location)
        {
            try
            {
                XElement xElement = XElement.Load(location);
                List<XElement> result = xElement.Elements().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't Read file from location: ", location);
                Console.WriteLine(ex);
            }
            return null;
        }

        public static bool WriteToFile(string location, List<string> data)
        {
            bool result = false;
            try
            {
                File.WriteAllLines(location, data);
                result = !result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't Write to file on location:", location);
                Console.WriteLine(ex);
            }
            return result;
        }

        public static string[] ScanDir(string location)
        {
            try
            {
                string[] dirs = Directory.GetDirectories(location);
                return dirs;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't Write to file on location:", location);
                Console.WriteLine(ex);
            }
            return null;
        }
    }
}