﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLSResourceManager
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Manager runnable = new Manager();
            runnable.Run();
        }
    }
}
